
package cl.az.test.client;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "enterprises"
})
public class EnterpriseResponse implements Serializable
{

    @JsonProperty("enterprises")
    private List<Enterprise> enterprises = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = 828057767855123292L;

    @JsonProperty("enterprises")
    public List<Enterprise> getEnterprises() {
        return enterprises;
    }

    @JsonProperty("enterprises")
    public void setEnterprises(List<Enterprise> enterprises) {
        this.enterprises = enterprises;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

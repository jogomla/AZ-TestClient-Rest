package cl.az.test.exception;

public class GetEnterpriseException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8125984980627634260L;
	public GetEnterpriseException(String message) {
		super(message);
	}
	public GetEnterpriseException(String message, Throwable t) {
		super(message, t);
	}

}

package cl.az.test.dao;

import java.util.Map;

import org.springframework.stereotype.Repository;

import cl.az.assets.integracion.rest.BaseRestImpl;
import cl.az.assets.integracion.rest.config.SimpleServiceConfig;
import cl.az.assets.integracion.rest.exception.ExternalServiceException;
import cl.az.test.client.EnterpriseResponse;
import cl.az.test.exception.GetEnterpriseException;

//Repository Hereda de component, anotaas la clase para que spring scanee los packages y la agregue en su contexto.
@Repository("getEnterpriseDao")
public class GetEnterpriseDaoImpl extends BaseRestImpl implements GetEnterpriseDao {

	@Override
	public EnterpriseResponse getEnterpriseByCompany(SimpleServiceConfig config, Map<String,Object> params) throws GetEnterpriseException{
		EnterpriseResponse resp = null;
		try {
			resp = get(config, EnterpriseResponse.class, params);
		} catch (ExternalServiceException e) {
			// TODO Auto-generated catch block
			// Aqui deberiamos hacer un log del error con alguna herramienta de Logger
			throw new GetEnterpriseException("Error consultando Servicio", e);
		}
		return resp;
	}
}

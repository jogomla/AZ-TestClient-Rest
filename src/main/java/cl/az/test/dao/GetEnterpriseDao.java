package cl.az.test.dao;

import java.util.Map;

import cl.az.assets.integracion.rest.config.SimpleServiceConfig;
import cl.az.test.client.EnterpriseResponse;
import cl.az.test.exception.GetEnterpriseException;

public interface GetEnterpriseDao {

	/* http://192.168.3.9:9083/cardifbackend/v1/enterprises.json/1 */
	EnterpriseResponse getEnterpriseByCompany(SimpleServiceConfig config, Map<String, Object> params)
			throws GetEnterpriseException;

}
package cl.az.test.dao;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cl.az.assets.integracion.rest.config.SimpleServiceConfig;
import cl.az.test.client.EnterpriseResponse;
import cl.az.test.exception.GetEnterpriseException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:app-ctx.xml"})
public class GetEnterpriseDaoImplTest {
	
	//private GetEnterpriseDaoImpl getEnterpriseDao = new GetEnterpriseDaoImpl();
	@Autowired
	@Qualifier("getEnterpriseDao")
	private GetEnterpriseDao getEnterpriseDao;
	private SimpleServiceConfig config;
	public Map<String,Object> params;

	@Before
	public void setUp() throws Exception {
		config = new SimpleServiceConfig();
		config.setEndpoint("http://192.168.3.9:9083/cardifbackend/v1/enterprises.json/{companyId}");
		
		params = new HashMap<String, Object>();
		params.put("companyId", 1);
	}

	@Test
	public void testGetEnterpriseByCompany() {
		EnterpriseResponse respuesta = null;
		try {
			respuesta = getEnterpriseDao.getEnterpriseByCompany(config, params);
		} catch (GetEnterpriseException e) {
			// TODO Auto-generated catch block
			fail("Fallo ejecutando Test:"+ e.getMessage());
		}
		
		assertNotNull(respuesta);
		assertNotNull(respuesta.getEnterprises());
		assertFalse(respuesta.getEnterprises().isEmpty());
		assertFalse("Tiene menos de 3 elementos el arreglo",respuesta.getEnterprises().size()<3);
		
		//fail("Not yet implemented");
	}

}
